<?php 

App::uses('AppModel', 'Model');

class Users extends AppModel {

	public $name = 'Users';
	
    public $useTable = 'users';

    public $primaryKey = 'id';

	public $belongsTo = array ();

	var $validate = array(
		'username' => array(
			'isUnique' => array (
				'rule' => 'isUnique',
				'message' => 'Usuário já cadastrado!'
			),
		)
	);
}
