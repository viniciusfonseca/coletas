<?php 

App::uses('AppModel', 'Model');

class Coletas extends AppModel {

	public $name = 'Coletas';
	
    public $useTable = 'coletas';

    public $primaryKey = 'id';

	public $belongsTo = array (
  'Empresa' => 
  array (
    'className' => 'Empresas',
    'foreignKey' => 'empresa',
  ),
  'Transportadora' => 
  array (
    'className' => 'Transportadoras',
    'foreignKey' => 'transportadora',
  ),
);
}
