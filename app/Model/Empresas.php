<?php 

App::uses('AppModel', 'Model');

class Empresas extends AppModel {

	public $name = 'Empresas';
	
    public $useTable = 'empresas';

    public $primaryKey = 'id';

	public $belongsTo = array (
);
}
