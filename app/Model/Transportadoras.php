<?php 

App::uses('AppModel', 'Model');

class Transportadoras extends AppModel {

	public $name = 'Transportadoras';
	
    public $useTable = 'transportadoras';

    public $primaryKey = 'id';

	public $belongsTo = array (
);
}
