<div class="box">
    <div class="box-header">
        <h3 class="box-title">Empresas</h3>
    </div>

    <div class="box-body table-responsive">

        <table id="datatable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Telefone</th>
                    <th>Estado</th>
                    <th>Cidade</th>
                    <th>Cep</th>
                    <th>Bairro</th>
                    <th>Endere&ccedil;o</th>
                    <th>N&uacute;mero</th>

                    <th>Editar</th>
                    <th>Deletar</th>
                </tr>
            </thead>

            <tbody>
                <?php  foreach ($empresas as $key =>
                $value) {  ?>
                <tr>
                    <td>
                        <?php  echo $value['Empresas']['id']; ?></td>
                    <td>
                        <?php  echo $value['Empresas']['nome']; ?></td>
                    <td>
                        <?php  echo $value['Empresas']['telefone']; ?></td>
                    <td>
                        <?php  echo $value['Empresas']['estado']; ?></td>
                    <td>
                        <?php  echo $value['Empresas']['cidade']; ?></td>
                    <td>
                        <?php  echo $value['Empresas']['cep']; ?></td>
                    <td>
                        <?php  echo $value['Empresas']['bairro']; ?></td>
                    <td>
                        <?php  echo $value['Empresas']['endereco']; ?></td>
                    <td>
                        <?php  echo $value['Empresas']['numero']; ?></td>

                    <td>
                        <a href="<?php echo $this->
                            Html->url(array("controller" => "empresas", "action" => "editar", $value['Empresas']['id'])); ?>">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo $this->
                            Html->url(array("controller" => "empresas", "action" => "deletar", $value['Empresas']['id'])); ?>">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
                <?php  }  ?></tbody>

        </table>

        <div class="form-group">
            <button type="bottom" onclick="window.location.href='<?php echo $this->
                Html->url(array('controller' => 'empresas', 'action' => 'cadastrar')); ?>'" class="btn btn-primary">Cadastrar
            </button>
        </div>

    </div>

</div>