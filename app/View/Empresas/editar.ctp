<div class="box ">

    <div class="box-header">
        <h3 class="box-title">Editar empresa</h3>
    </div>

    <?php echo $this->
    Form->create('Empresas', array('enctype' => 'multipart/form-data', 'url' => array('controller' => 'empresas', 'action' => 'editar'), 'class'=>'form') ); ?>
    <?php echo $this->
    Form->input('Empresas.id', array('type' => 'hidden', 'label'=>'')); ?>
    <div class="box-body">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Empresas.nome', array('type' => 'text', 'label'=>'Nome', 'class' => 'form-control', 'placeholder' => html_entity_decode(''), 'required' => 'required'));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Empresas.telefone', array('type' => 'text', 'label'=>'Telefone', 'class' => 'form-control telefone', 'placeholder' => html_entity_decode(''), 'required' => ''));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Empresas.estado', array('type' => 'text', 'label'=>'Estado', 'class' => 'form-control', 'placeholder' => html_entity_decode(''), 'required' => ''));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Empresas.cidade', array('type' => 'text', 'label'=>'Cidade', 'class' => 'form-control', 'placeholder' => html_entity_decode(''), 'required' => ''));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Empresas.cep', array('type' => 'text', 'label'=>'Cep', 'class' => 'form-control cep', 'placeholder' => html_entity_decode(''), 'required' => ''));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Empresas.bairro', array('type' => 'text', 'label'=>'Bairro', 'class' => 'form-control', 'placeholder' => html_entity_decode(''), 'required' => ''));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Empresas.endereco', array('type' => 'text', 'label'=>'Endere&ccedil;o', 'class' => 'form-control', 'placeholder' => html_entity_decode(''), 'required' => ''));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Empresas.numero', array('type' => 'text', 'label'=>'N&uacute;mero', 'class' => 'form-control', 'placeholder' => html_entity_decode(''), 'required' => ''));  ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    </div>

    <?php echo $this->Form->end();?></div>