<div class="box">
    <div class="box-header">
        <h3 class="box-title">Transportadoras</h3>
    </div>

    <div class="box-body table-responsive">

        <table id="datatable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Observa&ccedil;&atilde;o</th>

                    <th>Editar</th>
                    <th>Deletar</th>
                </tr>
            </thead>

            <tbody>
                <?php  foreach ($transportadoras as $key =>
                $value) {  ?>
                <tr>
                    <td>
                        <?php  echo $value['Transportadoras']['id']; ?></td>
                    <td>
                        <?php  echo $value['Transportadoras']['nome']; ?></td>
                    <td>
                        <?php  echo $value['Transportadoras']['observacao']; ?></td>

                    <td>
                        <a href="<?php echo $this->
                            Html->url(array("controller" => "transportadoras", "action" => "editar", $value['Transportadoras']['id'])); ?>">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo $this->
                            Html->url(array("controller" => "transportadoras", "action" => "deletar", $value['Transportadoras']['id'])); ?>">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
                <?php  }  ?></tbody>

        </table>

        <div class="form-group">
            <button type="bottom" onclick="window.location.href='<?php echo $this->
                Html->url(array('controller' => 'transportadoras', 'action' => 'cadastrar')); ?>'" class="btn btn-primary">Cadastrar
            </button>
        </div>

    </div>

</div>