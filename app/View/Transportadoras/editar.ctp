<div class="box ">

    <div class="box-header">
        <h3 class="box-title">Editar transportadora</h3>
    </div>

    <?php echo $this->
    Form->create('Transportadoras', array('enctype' => 'multipart/form-data', 'url' => array('controller' => 'transportadoras', 'action' => 'editar'), 'class'=>'form') ); ?>
    <?php echo $this->
    Form->input('Transportadoras.id', array('type' => 'hidden', 'label'=>'')); ?>
    <div class="box-body">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Transportadoras.nome', array('type' => 'text', 'label'=>'Nome', 'class' => 'form-control', 'placeholder' => html_entity_decode(''), 'required' => ''));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Transportadoras.observacao', array('type' => 'textarea', 'label'=>'Observa&ccedil;&atilde;o', 'class' => 'form-control', 'placeholder' => html_entity_decode(''), 'required' => ''));  ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    </div>

    <?php echo $this->Form->end();?></div>