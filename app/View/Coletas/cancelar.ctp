<div class="box ">

    <div class="box-header">
        <h3 class="box-title">Cancelamento</h3>
    </div>

    <?php echo $this->
    Form->create('Coletas', array('enctype' => 'multipart/form-data', 'url' => array('controller' => 'coletas', 'action' => 'cancelar'), 'class'=>'form') ); ?>
    <?php echo $this->
    Form->input('Coletas.id', array('type' => 'hidden', 'label'=>'')); ?>
    <div class="box-body">
        
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Coletas.empresa', array('type' => 'select', 'options' => $empresas_list, 'label'=>'Empresa', 'class' => 'form-control', 'placeholder' => '', 'required' => 'required', 'disabled'));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Coletas.transportadora', array('type' => 'select', 'options' => $transportadoras_list, 'label'=>'Transportadora', 'class' => 'form-control', 'placeholder' => '', 'required' => 'required', 'disabled'));  ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Coletas.data', array('type' => 'text', 'label'=>'Data', 'class' => 'form-control data', 'placeholder' => html_entity_decode(''), 'required' => 'required', 'disabled'));  ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Coletas.observacao', array('type' => 'textarea', 'label'=>'Motivo', 'class' => 'form-control', 'placeholder' => html_entity_decode(''), 'required' => 'required'));  ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Cancelar</button>
        </div>
    </div>

    <?php echo $this->Form->end();?></div>