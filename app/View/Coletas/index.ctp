<style>
    
    a .glyphicon{  
        float: left;
        padding: 5px;
    }
</style>

<div class="modal fade" id="myModalCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Empresa</h4>
      </div>
      <div class="modal-body">
            <h4>Nome: <span id="empresa_nome"></span></h4>
            <h4>Telefone: <span id="empresa_telefone"></span></h4>
            <h4>Estado: <span id="empresa_estado"></span></h4>
            <h4>Cidade: <span id="empresa_cidade"></span></h4>
            <h4>Cep: <span id="empresa_cep"></span></h4>
            <h4>Bairro: <span id="empresa_bairro"></span></h4>
            <h4>Endereço: <span id="empresa_endereco"></span></h4>
            <h4>Número: <span id="empresa_numero"></span></h4>
      </div>
    </div>
  </div>
</div>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Coletas</h3>
    </div>

    <div class="box-body table-responsive">

        <table id="datatable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Status</th>
                    <th>Empresa</th>
                    <th>Transportadora</th>
                    <th>Data</th>
                    <th>Descri&ccedil;&atilde;o</th>
                    <th>HR - Pronto</th>
                    <th>HR - Final</th>
                    <th>Última modificação</th>
                    <th>Solicitante</th>
                    <th>Motorista</th>

                    <th></th>
                </tr>
            </thead>

            <tbody>
                <?php  foreach ($coletas as $key =>
                $value) {  ?>
                <tr>
                    <td>
                        <?php  echo $value['Coletas']['id']; ?></td>

                    <td>
                        <?php if ($value['Coletas']['status'] == 1) {
                            echo 'COLETA EM ABERTO';
                        }elseif ($value['Coletas']['status'] == 2) {
                            echo 'COLETA INFORMADA';
                        } ?>
                    </td>

                    <td>
                        <a href="javascript:void(0);" class="myModalCliente" id="<?php  echo $value['Empresa']['id'];  ?>"><?php  echo $value['Empresa']['nome'];  ?></a></td>
                    <td>
                        <?php  echo $value['Transportadora']['nome'];  ?></td>
                    <td>
                        <?php  echo $value['Coletas']['data']; ?></td>
                    <td>
                        <?php  echo $value['Coletas']['descricao']; ?></td>
                    <td>
                        <?php  echo (isset($value['Coletas']['hrpronto'])) ? date('d/m/Y H:i:s', strtotime($value['Coletas']['hrpronto'])) : '';  ?></td>
                    <td>
                        <?php  echo (isset($value['Coletas']['hrfinal'])) ? date('d/m/Y H:i:s', strtotime($value['Coletas']['hrfinal'])) : '';  ?></td>
                    <td>
                        <?php  echo $value['Coletas']['usuario_log']; ?></td>
                    <td>
                        <?php  echo $value['Coletas']['solicitante']; ?></td>
                    <td>
                        <?php  echo $value['Coletas']['motorista']; ?></td>
                    <td>
                        <a href="<?php echo $this->
                            Html->url(array("controller" => "coletas", "action" => "cancelar", $value['Coletas']['id'])); ?>" alt="Cancelar coleta" title="Cancelar coleta">
                            <span class="glyphicon glyphicon-thumbs-down"></span>
                        </a>
                        <a href="<?php echo $this->
                            Html->url(array("controller" => "coletas", "action" => "aprovar", $value['Coletas']['id'])); ?>" alt="Confirmar coleta" title="Confirmar coleta">
                            <span class="glyphicon glyphicon-hand-left"></span>
                        </a>
                        <a href="<?php echo $this->
                            Html->url(array("controller" => "coletas", "action" => "editar", $value['Coletas']['id'])); ?>" alt="Editar coleta" title="Editar coleta">
                            <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        <a href="<?php echo $this->
                            Html->url(array("controller" => "coletas", "action" => "deletar", $value['Coletas']['id'])); ?>" alt="Deletar coleta" title="Deletar coleta">
                            <span class="glyphicon glyphicon-trash"></span>
                        </a>
                    </td>
                </tr>
                <?php  }  ?></tbody>

        </table>

        <div class="form-group">
            <button type="bottom" onclick="window.location.href='<?php echo $this->
                Html->url(array('controller' => 'coletas', 'action' => 'cadastrar')); ?>'" class="btn btn-primary">Cadastrar
            </button>
        </div>

    </div>

</div>