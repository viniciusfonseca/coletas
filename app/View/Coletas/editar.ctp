<div class="box ">

    <div class="box-header">
        <h3 class="box-title">Editar coleta</h3>
    </div>

    <?php echo $this->
    Form->create('Coletas', array('enctype' => 'multipart/form-data', 'url' => array('controller' => 'coletas', 'action' => 'editar'), 'class'=>'form') ); ?>
    <?php echo $this->
    Form->input('Coletas.id', array('type' => 'hidden', 'label'=>'')); ?>
    <div class="box-body">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Coletas.empresa', array('type' => 'select', 'options' => $empresas_list, 'label'=>'Empresa', 'class' => 'form-control', 'placeholder' => '', 'required' => 'required'));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Coletas.transportadora', array('type' => 'select', 'options' => $transportadoras_list, 'label'=>'Transportadora', 'class' => 'form-control', 'placeholder' => '', 'required' => 'required'));  ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Coletas.solicitante', array('type' => 'text', 'label'=>'Solicitante', 'class' => 'form-control', 'placeholder' => html_entity_decode(''), 'required' => ''));  ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Coletas.data', array('type' => 'text', 'label'=>'Data', 'class' => 'form-control data', 'placeholder' => html_entity_decode(''), 'required' => 'required'));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Coletas.descricao', array('type' => 'textarea', 'label'=>'Descri&ccedil;&atilde;o', 'class' => 'form-control', 'placeholder' => html_entity_decode(''), 'required' => 'required'));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Coletas.hrpronto', array('type' => 'text', 'label'=>'HR - Pronto', 'class' => 'form-control componenteDataHora', 'placeholder' => html_entity_decode(''), 'required' => 'required'));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Coletas.hrfinal', array('type' => 'text', 'label'=>'HR - Final', 'class' => 'form-control componenteDataHora', 'placeholder' => html_entity_decode(''), 'required' => 'required'));  ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->
                    Form->input('Coletas.motorista', array('type' => 'text', 'label'=>'Motorista', 'class' => 'form-control', 'placeholder' => html_entity_decode(''), 'required' => ''));  ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    </div>

    <?php echo $this->Form->end();?></div>