<div class="box">
    <div class="box-header">
        <h3 class="box-title">Coletas canceladas</h3>
    </div>

    <div class="box-body table-responsive">

        <table id="datatable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Empresa</th>
                    <th>Transportadora</th>
                    <th>Data</th>
                    <th>Descri&ccedil;&atilde;o</th>
                    <th>HR - Pronto</th>
                    <th>HR - Final</th>
                    <th>Motorista</th>
                    <th>Observa&ccedil;&atilde;o</th>
                </tr>
            </thead>

            <tbody>
                <?php  foreach ($coletas as $key =>
                $value) {  ?>
                <tr>
                    <td>
                        <?php  echo $value['Coletas']['id']; ?></td>
                    <td>
                        <?php  echo $value['Empresa']['nome'];  ?></td>
                    <td>
                        <?php  echo $value['Transportadora']['nome'];  ?></td>
                    <td>
                        <?php  echo $value['Coletas']['data']; ?></td>
                    <td>
                        <?php  echo $value['Coletas']['descricao']; ?></td>
                    <td>
                        <?php  echo (isset($value['Coletas']['hrpronto'])) ? date('d/m/Y H:i:s', strtotime($value['Coletas']['hrpronto'])) : '';  ?></td>
                    <td>
                        <?php  echo (isset($value['Coletas']['hrfinal'])) ? date('d/m/Y H:i:s', strtotime($value['Coletas']['hrfinal'])) : '';  ?></td>
                    <td>
                        <?php  echo $value['Coletas']['motorista']; ?></td>
                    <td>
                        <?php  echo $value['Coletas']['observacao']; ?></td>
                </tr>
                <?php  }  ?></tbody>

        </table>
    </div>

</div>