<!DOCTYPE html>
<html>

<head>

    <meta charset="UTF-8">

    <title>Sistema de gestão | Coletas</title>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <link rel="icon" type="image/x-icon" href="<?php echo $this->
    webroot;?>img/favicon.png"/>
    <link rel="stylesheet" href="<?php echo $this->
    webroot;?>css/AdminLTE.css">
    <link rel="stylesheet" href="<?php echo $this->
    webroot;?>css/datepicker.css">
    <link rel="stylesheet" href="<?php echo $this->
    webroot;?>css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.7/css/jquery.dataTables.css">

    <link rel="stylesheet" href="<?php echo $this->
    webroot;?>css/jquery.fancybox.css">
    <!-- <link rel="stylesheet" href="http://cdn.datatables.net/tabletools/2.2.1/css/dataTables.tableTools.min.css">
    -->
    <style>
            .jumbotron{
                padding: 30px;
                background-color: #fff;
                margin-bottom:0;
            }

            #datatable{
                margin-bottom: 20px;
            }
        </style>

</head>

<div class="loader" style="height:100%; width:100%; position:fixed;left:0;top:0;z-index:9000;  background:#000; opacity:0.8; display:none;">
    <div style="width:600px; height:600px;  position:fixed;left:45%;top:50%;z-index:9000;">
        <img src="<?php echo $this->webroot;?>img/loader-barra.gif"/>
    </div>
</div>

<body class="skin-blue">
    <header class="header">
        <a href="<?php echo $this->
            Html->url(array('controller' => 'pages', 'action' => 'display')); ?>" class="logo">
                Coletas
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <div class="navbar-right">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-user"></i>
                            <span> <i class="caret"></i>
                            </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="<?php echo $this->
                                        Html->url(array('controller' => 'users', 'action' => 'logout')); ?>" class="btn btn-default btn-flat">Sair
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <aside class="left-side sidebar-offcanvas">
            <section class="sidebar">
                <ul id="menu" class="sidebar-menu">

                    <li class="active" id="principal">
                        <a href="<?php echo $this->Html->url(array('controller' => 'pages', 'action' => 'index')); ?>">
                            <i class="fa fa-folder"></i>
                            <span>Principal</span>
                        </a>
                    </li>

                    <li id="empresas">
                        <a href="<?php  echo $this->Html->url(array('controller' => 'empresas', 'action' => 'index'));  ?>" title="">
                            <i class="fa fa-folder"></i>
                            <span>Clientes</span>
                        </a>
                    </li>

                    <li id="transportadoras">
                        <a href="<?php  echo $this->Html->url(array('controller' => 'transportadoras', 'action' => 'index'));  ?>" title="">
                            <i class="fa fa-folder"></i>
                            <span>Transportadoras</span>
                        </a>
                    </li>

                    <li id="coletas">
                        <a href="<?php echo $this->Html->url(array('controller' => 'coletas', 'action' => 'index'));  ?>" title="">
                            <i class="fa fa-folder"></i>
                            <span>Coletas</span>
                        </a>
                    </li>

                    <li id="coletas-efetuadas">
                        <a href="<?php  echo $this->Html->url(array('controller' => 'coletas', 'action' => 'efetuadas'));  ?>" title="">
                            <i class="fa fa-folder"></i>
                            <span>Coletas efetuadas</span>
                        </a>
                    </li>

                    <li id="coletas-canceladas">
                        <a href="<?php  echo $this->Html->url(array('controller' => 'coletas', 'action' => 'canceladas'));  ?>" title="">
                            <i class="fa fa-folder"></i>
                            <span>Coletas canceladas</span>
                        </a>
                    </li>

                    <li class="active" id="principal">
                        <a href="<?php echo $this->Html->url(array('controller' => 'users', 'action' => 'index')); ?>">
                            <i class="fa fa-folder"></i>
                            <span>Usuários</span>
                        </a>
                    </li>

                </ul>
            </section>
        </aside>

        <aside class="right-side">

            <?php echo $this->
            Session->flash(); ?>
            <section class="content">

                <?php echo $this->fetch('content'); ?></section>

        </aside>
    </div>

    <footer style="margin-bottom: 10px; width: 100%; text-align: center;">© 2016 - Todos os direitos reservados</footer>

    <script type="text/javascript" src="<?php echo $this->webroot;?>js/jquery-2.0.3.min.js"></script>

    <script type="text/javascript" src="<?php echo $this->webroot;?>js/jquery.maskedinput.min.js"></script>

    <script type="text/javascript" src="<?php echo $this->webroot;?>js/bootstrap-datepicker.js"></script>

    <script type="text/javascript" src="<?php echo $this->webroot;?>js/lib/tinymce/js/tinymce/tinymce.min.js"></script>

    <script type="text/javascript" src="<?php echo $this->webroot;?>js/jquery.fancybox.js"></script>

    <script src="<?php echo $this->webroot;?>js/bootstrap-datetimepicker.min.js"></script>

    <script src="<?php echo $this->webroot;?>js/bootstrap.min.js"></script>

    <script src="<?php echo $this->webroot;?>js/app.js"></script>

    <script src="<?php echo $this->webroot;?>js/jquery.maskMoney.min.js"></script>

    <script src="<?php echo $this->webroot;?>js/chosen.jquery/js/chosen.jquery.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript" src="<?php echo $this->webroot;?>js/jquery.fancybox.js"></script>

    <!-- <script type="text/javascript" src="http://cdn.datatables.net/tabletools/2.2.1/js/dataTables.tableTools.min.js"></script>
-->
<script type="text/javascript">

        $(document).ready(function() {

            var controller = "<?php echo $this->params['controller']; ?>";

            $("#menu li").each(function(e) { 

                if ($(this).attr('id') == controller) {
                    $(this).addClass('active');
                } else {
                    if ($(this).hasClass('active')) {
                        $(this).removeClass('active');
                    }
                }
            });

            $(".componenteData").each(function(e) { 

                var date = $(this).val();

                if (date.length > 0) { 
                   
                    var dateSplit = date.split("-");
                    
                    var dateConvert = dateSplit[2] + '/' + dateSplit[1] + '/' + dateSplit[0];

                    $(this).val(dateConvert);
                }

            });
            
            $('.componenteData').datepicker({
                format: 'dd/mm/yyyy',
                language: 'pt-BR'
            });

            $(".componenteDataHora").each(function(e) {

                var date = $(this).val();

                if (date.length > 0) { 
                   
                    var dataHora = date.split(" ");

                    var dateSplit = dataHora[0].split("-");
                    
                    var dateConvert = dateSplit[2] + '/' + dateSplit[1] + '/' + dateSplit[0];

                    $(this).val(dateConvert + " " + dataHora[1].substr(0, 5));
                }

            });

            $(function() {
                $('.componenteDataHora').datetimepicker({
                    format: 'dd/mm/yyyy hh:ii',
                    autoclose: true
                });
            });

            $("form").submit(function(){
                
                $(".componenteData").each(function(e) { 

                    var date = $(this).val();

                    if (date.length > 0) { 
                       
                        var dateSplit = date.split("/");

                        var dateConvert = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0];

                        $(this).val(dateConvert);
                    }
                });

                $(".componenteDataHora").each(function(e) {

                    var date = $(this).val();

                    if (date.length > 0) { 
                       
                        var dataHora = date.split(" ");

                        var dateSplit = dataHora[0].split("/");
                        
                        var dateConvert = dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0];

                        $(this).val(dateConvert + " " + dataHora[1] + ":00");
                    }

                });
            });

            $(".money").maskMoney();

            $('.cep').mask('99999-999');

            $('.data').mask('99/99/9999');

            $('.cpf').mask('999.999.999-99');

            $('.cnpj').mask('99.999.999/9999-99');

            $('.telefone').focusout(function(){
                var phone, element;
                element = $(this);
                element.unmask();
                phone = element.val().replace(/\D/g, '');
                if(phone.length > 10) {
                    element.mask("(99) 99999-999?9");
                } else {
                    element.mask("(99) 9999-9999?9");
                }
            }).trigger('focusout');

            $(".fancybox").fancybox({
                openEffect  : 'none',
                closeEffect : 'none'
            });

            tinymce.init({
                selector: "textarea.tinymce",
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste "
                ],
                plugin_preview_width : "900",
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            });

            var table = $('#datatable').DataTable( {
                "scrollX": true,
                "dom": 'T<"clear">lfrtip',
                // "tableTools": {
                //     "sSwfPath": '<?php echo $this->webroot;?>swf/copy_csv_xls_pdf.swf',
                //     "aButtons": [
                //         {
                //             "sExtends":       "collection",
                //             "sButtonText": "Exportar",
                //             "aButtons":    [ "xls", "pdf" ]
                //         }
                //     ]
                // },
                "oLanguage": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                }
            } );

        } );

        $(".myModalCliente").click(function(event){

            $(".loader").show();

            var cliente = $(this).attr('id');

            $.ajax({
                type: "post",
                url: "empresas/ajax/" + cliente,
                dataType: 'json',
                success: function(response){   

                    $("#empresa_nome").html(response.Empresas.nome);
                    $("#empresa_telefone").html(response.Empresas.telefone);
                    $("#empresa_estado").html(response.Empresas.estado);
                    $("#empresa_cidade").html(response.Empresas.cidade);
                    $("#empresa_cep").html(response.Empresas.cep);
                    $("#empresa_bairro").html(response.Empresas.bairro);
                    $("#empresa_endereco").html(response.Empresas.endereco);
                    $("#empresa_numero").html(response.Empresas.numero);

                    $('#myModalCliente').modal('show');

                    $(".loader").hide();
                }
            });
        });

    </script>

</body>

</html>