<div class="box">
    <div class="box-header">
        <h3 class="box-title">Usuários cadastrados</h3>
    </div>

    <div class="box-body table-responsive">

        <table id="datatable" class="display table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Usuário</th>
                    <th>Senha</th>
                    <th>Perfil</th>
                    <!-- <th>Status</th>-->
                    <th>Data de criação</th>
                    <th>Última atualização</th>
                    <th>Editar</th>
                    <th>Deletar</th>
                </tr>
            </thead>

        <tbody>
            <?php  foreach ($users as $key => $value) {  ?>
            <tr>
                <td>
                    <?php  echo $value['Users']['id'];  ?></td>
                <td>
                    <?php  echo $value['Users']['username'];  ?></td>
                <td>
                    ****** </td>
                <td>
                    <?php  echo ($value['Users']['role'] == 'admin') ? 'Administrador' : $value['Users']['role'];  ?></td>
                <td>
                    <?php  echo (isset($value['Users']['created'])) ? date('d/m/Y H:i:s', strtotime($value['Users']['created'])) : '';  ?></td>
                <td>
                    <?php  echo (isset($value['Users']['modified'])) ? date('d/m/Y H:i:s', strtotime($value['Users']['modified'])) : '';  ?></td>

                <td>
                    <a href="<?php echo $this->Html->url(array("controller" => "users", "action" => "editar", $value['Users']['id'])); ?>">
                        <span class="glyphicon glyphicon-edit"></span>
                    </a>
                </td>
                <td>
                    <a href="<?php echo $this->Html->url(array("controller" => "users", "action" => "deletar", $value['Users']['id'])); ?>">
                        <span class="glyphicon glyphicon-trash"></span>
                    </a>
                </td>
            </tr>
            <?php  }  ?></tbody>

    </table>

    <div class="form-group">
        <button type="bottom" onclick="window.location.href='<?php echo $this->
            Html->url(array('controller' => 'users', 'action' => 'cadastrar')); ?>'" class="btn btn-primary">Cadastrar
        </button>
    </div>

</div>

</div>