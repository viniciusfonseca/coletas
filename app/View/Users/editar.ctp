<div class="box ">

    <div class="box-header">
        <h3 class="box-title">Editar usuário</h3>
    </div>

    <?php echo $this->Form->create('Users', array('enctype' => 'multipart/form-data', 'url' => array('controller' => 'users', 'action' => 'editar'), 'class'=>'form') ); ?>

    <?php echo $this->Form->input('Users.id', array('type' => 'hidden', 'label'=>'')); ?>

    <div class="box-body">

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->Form->input('Users.username', array('type' => 'text', 'label'=>'Usuário', 'class' => 'form-control', 'placeholder' => '', 'required' => 'required'));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->Form->input('Users.password', array('type' => 'password', 'label'=>'Senha', 'class' => 'form-control', 'placeholder' => '', 'required' => 'required', 'value' => '', 'placeholder' => '******'));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->Form->input('Users.role', array('type' => 'select', 'options' => array('admin' => 'Administrador', 'colaborador' => 'Colaborador'), 'label'=>'Perfil', 'class' => 'form-control', 'placeholder' => '', 'required' => ''));  ?>
                </div>
            </div>
        </div>
        <!-- <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->Form->input('Users.status', array('type' => 'checkbox', 'label'=>'Ativo?', 'class' => 'form-control', 'placeholder' => '', 'required' => ''));  ?>
                </div>
            </div>
        </div> -->
        <!-- <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->Form->input('Users.created', array('type' => 'text', 'label'=>'Created', 'class' => 'form-control componenteDataHora', 'placeholder' => '', 'required' => ''));  ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?php  echo $this->Form->input('Users.modified', array('type' => 'text', 'label'=>'Modified', 'class' => 'form-control componenteDataHora', 'placeholder' => '', 'required' => ''));  ?>
                </div>
            </div>
        </div> -->

        <div class="form-group">
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    </div>

    <?php echo $this->Form->end(); ?>

</div>