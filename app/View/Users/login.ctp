<!DOCTYPE html>
<html class="bg-black">
    <head>

        <meta charset="UTF-8">

        <title> Coletas | Login </title>

        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
       
        <link rel="stylesheet" href="<?php echo $this->webroot;?>css/AdminLTE.css">

        <link rel="icon" type="image/x-icon" href="<?php echo $this->webroot;?>img/favicon.png"/>

        <link rel="stylesheet" href="<?php echo $this->webroot;?>css/bootstrap.min.css">

        <link rel="stylesheet" href="<?php echo $this->webroot;?>css/AdminLTE.css">

         <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    </head>

    <body class="bg-black">

        <div class="form-box" id="login-box">
            
            <?php echo $this->Session->flash(); ?>
            
            <div class="header">Login</div>
            
             <?php echo $this->Form->create('User', array('action' => 'login', 'id' => 'validate', 'class' => 'form-signin')); ?>
               
                <div class="body bg-gray">

                    <div class="form-group">
                        <?php echo $this->Form->input('User.username', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Usuário', 'value' => 'demo', 'label'=> 'Usuário')); ?>     
                    </div>

                    <div class="form-group">
                        <?php echo $this->Form->input('User.password', array('type' => 'password', 'class' => 'form-control', 'placeholder' => 'Senha', 'value' => 'demo', 'label'=> 'Senha')); ?>     
                    </div>

                </div>

                <div class="footer">
                    <button type="submit" class="btn bg-olive btn-block">Entrar</button>
                </div>

            </form>

        </div> 

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js" type="text/javascript"></script>

    </body>
</html>