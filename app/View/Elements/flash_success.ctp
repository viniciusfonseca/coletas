<div class="pad margin no-print">
    <div class="alert alert-success" style="margin-bottom: 0!important;">
    <button type="button" class="close" data-dismiss="alert">×</button>
        <i class="fa fa-info"></i>
        <b><?php echo $message; ?></b>
    </div>
</div>