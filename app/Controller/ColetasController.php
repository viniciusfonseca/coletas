<?php 

App::uses('AppController', 'Controller');

class ColetasController extends AppController {

	var $uses = array('Coletas' ,'Empresas','Transportadoras');

	public function index() {
		
		$coletas = $this->Coletas->find('all', array('conditions' => array('status' => array(1,2))));

		$this->set('coletas', $coletas);
	}

	public function canceladas() {
		
		$coletas = $this->Coletas->find('all', array('conditions' => array('status' => 4)));

		$this->set('coletas', $coletas);
	}

	public function efetuadas() {
		
		$coletas = $this->Coletas->find('all', array('conditions' => array('status' => 3)));

		$this->set('coletas', $coletas);
	}

	public function cadastrar() {

		if (!empty($this->data)) {

			try {

                $db = $this->Coletas->getDataSource();

                $db->begin();

                $data = $this->data;

                $user = $this->Auth->user();

                $data['Coletas']['usuario_log'] = ucwords($user['username']);
				
				if ($this->Coletas->save($data)) {
					$this->Session->setFlash(__('Coleta cadastrada com sucesso!'), 'flash_success');
				} else {
					throw new Exception('Erro ao cadastrar coleta!');
				}

				$db->commit();

				$this->redirect(array('action' => 'index'));

            } catch (Exception $e) {

                $db->rollback();
                
                $this->Session->setFlash(__($e->getMessage()), 'flash_error');

            }
		}

		$empresas_list = $this->Empresas->find("list", array("fields" => array("id", "nome")));
		$transportadoras_list = $this->Transportadoras->find("list", array("fields" => array("id", "nome")));

		$this->set('empresas_list', $empresas_list);
		$this->set('transportadoras_list', $transportadoras_list);

	}

	public function editar($id) {

		if (!empty($this->data)) {

			try {

                $db = $this->Coletas->getDataSource();

                $db->begin();

				$data = $this->data;

				$user = $this->Auth->user();

                $data['Coletas']['usuario_log'] = ucwords($user['username']);

				if ($data['Coletas']['motorista'] !== '') {
					$data['Coletas']['status'] = 2;
				} elseif ($data['Coletas']['motorista'] == '') {
					$data['Coletas']['status'] = 1;
				}

				if ($this->Coletas->save($data)) {
					$this->Session->setFlash(__('Coleta atualizada com sucesso!'), 'flash_success');
				} else {
					throw new Exception('Erro ao atualizar coleta!');
				}

				$db->commit();

				$this->redirect(array('action' => 'index'));

            } catch (Exception $e) {

                $db->rollback();
                
                $this->Session->setFlash(__($e->getMessage()), 'flash_error');

            }
		} else {
			$this->data = $this->Coletas->read(null, $id);
		}
		
		$empresas_list = $this->Empresas->find("list", array("fields" => array("id", "nome")));
		$transportadoras_list = $this->Transportadoras->find("list", array("fields" => array("id", "nome")));


		$this->set('empresas_list', $empresas_list);
		$this->set('transportadoras_list', $transportadoras_list);

	}

	public function aprovar($id) {

		if ($id) {

			try {

				$this->data = $this->Coletas->read(null, $id);

				if ($this->data['Coletas']['motorista'] == '') {
					throw new Exception('Motorista não informado para a coleta!');
				}

				$data['Coletas']['status'] = 3;

				if ($this->Coletas->save($data)) {
					$this->Session->setFlash(__('Coleta confirmada com sucesso!'), 'flash_success');
				} else {
					throw new Exception('Erro ao confirmar coleta!');
				}

            } catch (Exception $e) {
                
                $this->Session->setFlash(__($e->getMessage()), 'flash_error');

            }
		}

		$this->redirect(array('controller' => 'coletas', 'action' => 'index'));
	
	}

	public function cancelar($id) {

		if (!empty($this->data)) {

			try {

                $db = $this->Coletas->getDataSource();

                $db->begin();

				$data = $this->data;

				$data['Coletas']['status'] = 4;

				if ($this->Coletas->save($data)) {
					$this->Session->setFlash(__('Coleta cancelada com sucesso!'), 'flash_success');
				} else {
					throw new Exception('Erro ao cancelar coleta!');
				}

				$db->commit();

				$this->redirect(array('action' => 'index'));

            } catch (Exception $e) {

                $db->rollback();
                
                $this->Session->setFlash(__($e->getMessage()), 'flash_error');

            }
		} else {
			$this->data = $this->Coletas->read(null, $id);
		}

		$empresas_list = $this->Empresas->find("list", array("fields" => array("id", "nome")));
		$transportadoras_list = $this->Transportadoras->find("list", array("fields" => array("id", "nome")));


		$this->set('empresas_list', $empresas_list);
		$this->set('transportadoras_list', $transportadoras_list);
	}
	
	public function deletar($id) {

		try {

            $db = $this->Coletas->getDataSource();

            $db->begin();

			if ($this->Coletas->delete($id, true)) {
				$this->Session->setFlash(__('Coleta deletada com sucesso!'), 'flash_success');
			} else {
				throw new Exception('Erro ao deletar coleta!');
			}

			$db->commit();

        } catch (Exception $e) {

            $db->rollback();
            
            $this->Session->setFlash(__($e->getMessage()), 'flash_error');
        }
        
		$this->redirect(array('action' => 'index'));
	}
}
