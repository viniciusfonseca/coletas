<?php 

App::uses('AppController', 'Controller');

class UsersController extends AppController {

	var $uses = array('Users');

	public function index() {
		
		$users = $this->Users->find('all');

		$this->set('users', $users);
	}

	public function cadastrar() {

		if (!empty($this->data)) {

			try {

                $db = $this->Users->getDataSource();

                $db->begin();

                $data = $this->data;

                $data['Users']['password'] = $this->Auth->password($this->data['Users']['password']);

                $data['Users']['status'] = true;

				if ($this->Users->save($data)) {
					$this->Session->setFlash(__('Usuário cadastrado com sucesso!'), 'flash_success');
				} else {
					throw new Exception('Erro ao cadastrar usuário!');
				}

				$db->commit();

				$this->redirect(array('action' => 'index'));

            } catch (Exception $e) {

                $db->rollback();
                
                $this->Session->setFlash(__($e->getMessage()), 'flash_error');

            }
		}
	}

	public function editar($id) {

		if (!empty($this->data)) {

			try {

                $db = $this->Users->getDataSource();

                $db->begin();

                $data = $this->data;

                $data['Users']['password'] = $this->Auth->password($this->data['Users']['password']);

				if ($this->Users->save($data)) {
					$this->Session->setFlash(__('Usuário atualizado com sucesso!'), 'flash_success');
				} else {
					throw new Exception('Erro ao atualizar usuário!');
				}

				$db->commit();

				$this->redirect(array('action' => 'index'));

            } catch (Exception $e) {

                $db->rollback();
                
                $this->Session->setFlash(__($e->getMessage()), 'flash_error');

            }
		} else {
			$this->data = $this->Users->read(null, $id);
		}
	}
	
	public function deletar($id) {

		try {

            $db = $this->Users->getDataSource();

            $db->begin();

			if ($this->Users->delete($id, true)) {
				$this->Session->setFlash(__('Usuário deletado com sucesso!'), 'flash_success');
			} else {
				throw new Exception('Erro ao deletar usuário!');
			}

			$db->commit();

        } catch (Exception $e) {

            $db->rollback();
            
            $this->Session->setFlash(__($e->getMessage()), 'flash_error');
        }
        
		$this->redirect(array('action' => 'index'));
	}

	public function login() {
		
	    if ($this->request->is('post')) {

	        if ($this->Auth->login()) {
	            $this->redirect(array('controller' => 'pages', 'action' => 'display'));
	        } else {
	            $this->Session->setFlash(__('Usuário e senha não coincidem!'), 'flash_error');
	        }
	    }

	    $this->layout = false;
	}

	public function logout() {
	    $this->redirect($this->Auth->logout());
	}
}
