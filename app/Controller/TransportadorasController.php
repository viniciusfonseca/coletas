<?php 

App::uses('AppController', 'Controller');

class TransportadorasController extends AppController {

	var $uses = array('Transportadoras' ,);

	public function index() {
		
		$transportadoras = $this->Transportadoras->find('all');

		$this->set('transportadoras', $transportadoras);
	}

	public function cadastrar() {

		if (!empty($this->data)) {

			try {

                $db = $this->Transportadoras->getDataSource();

                $db->begin();

                $data = $this->data;

				if ($this->Transportadoras->save($data)) {
					$this->Session->setFlash(__('Transportadora cadastrada com sucesso!'), 'flash_success');
				} else {
					throw new Exception('Erro ao cadastrar transportadora!');
				}

				$db->commit();

				$this->redirect(array('action' => 'index'));

            } catch (Exception $e) {

                $db->rollback();
                
                $this->Session->setFlash(__($e->getMessage()), 'flash_error');

            }
		}
	}

	public function editar($id) {

		if (!empty($this->data)) {

			try {

                $db = $this->Transportadoras->getDataSource();

                $db->begin();

				$data = $this->data;
				
				if ($this->Transportadoras->save($data)) {
					$this->Session->setFlash(__('Transportadora atualizada com sucesso!'), 'flash_success');
				} else {
					throw new Exception('Erro ao atualizar transportadora!');
				}

				$db->commit();

				$this->redirect(array('action' => 'index'));

            } catch (Exception $e) {

                $db->rollback();
                
                $this->Session->setFlash(__($e->getMessage()), 'flash_error');

            }
		} else {
			$this->data = $this->Transportadoras->read(null, $id);
		}
	}
	
	public function deletar($id) {

		try {

            $db = $this->Transportadoras->getDataSource();

            $db->begin();

			if ($this->Transportadoras->delete($id, true)) {
				$this->Session->setFlash(__('Transportadora deletada com sucesso!'), 'flash_success');
			} else {
				throw new Exception('Erro ao deletar transportadora!');
			}

			$db->commit();

        } catch (Exception $e) {

            $db->rollback();
            
            $this->Session->setFlash(__($e->getMessage()), 'flash_error');
        }
        
		$this->redirect(array('action' => 'index'));
	}
}
