<?php 

App::uses('AppController', 'Controller');

class EmpresasController extends AppController {

	var $uses = array('Empresas' ,);

	public function index() {
		
		$empresas = $this->Empresas->find('all');

		$this->set('empresas', $empresas);
	}

	public function cadastrar() {

		if (!empty($this->data)) {

			try {

                $db = $this->Empresas->getDataSource();

                $db->begin();

                $data = $this->data;
				
				if ($this->Empresas->save($data)) {
					$this->Session->setFlash(__('Empresa cadastrada com sucesso!'), 'flash_success');
				} else {
					throw new Exception('Erro ao cadastrar empresa!');
				}

				$db->commit();

				$this->redirect(array('action' => 'index'));

            } catch (Exception $e) {

                $db->rollback();
                
                $this->Session->setFlash(__($e->getMessage()), 'flash_error');

            }
		}
		
	}

	public function editar($id) {

		if (!empty($this->data)) {

			try {

                $db = $this->Empresas->getDataSource();

                $db->begin();

				$data = $this->data;
				
				if ($this->Empresas->save($data)) {
					$this->Session->setFlash(__('Empresa atualizada com sucesso!'), 'flash_success');
				} else {
					throw new Exception('Erro ao atualizar empresa!');
				}

				$db->commit();

				$this->redirect(array('action' => 'index'));

            } catch (Exception $e) {

                $db->rollback();
                
                $this->Session->setFlash(__($e->getMessage()), 'flash_error');

            }
		} else {
			$this->data = $this->Empresas->read(null, $id);
		}
		
	}
	
	public function deletar($id) {

		try {

            $db = $this->Empresas->getDataSource();

            $db->begin();

			if ($this->Empresas->delete($id, true)) {
				$this->Session->setFlash(__('Empresa deletada com sucesso!'), 'flash_success');
			} else {
				throw new Exception('Erro ao deletar empresa!');
			}

			$db->commit();

        } catch (Exception $e) {

            $db->rollback();
            
            $this->Session->setFlash(__($e->getMessage()), 'flash_error');
        }
        
		$this->redirect(array('action' => 'index'));
	}

	public function ajax($id) {

		$this->render(false, false);

		$result = $this->Empresas->read(null, $id);

		echo json_encode($result);
	}
}
